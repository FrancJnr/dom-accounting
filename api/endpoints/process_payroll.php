<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$path_to_root = "../../";
include($path_to_root . "api/db/add_gl_trans.php");
include("../db/connect_db.php");

$action = isset($_GET['action']) ? $_GET['action']: die();
$db = new Database();
$authenticated = 1;
$gl = new Gl($db->getConnection());

if($authenticated!= -1){
    if($action=='process-payroll'){
        $json = file_get_contents('php://input');
        $data = json_decode($json); 
        foreach($data as $payrollItem){
            $typeNo = $gl->add_to_journal(0,  "", "",  $payrollItem->Ref, "",  "", "", "KS", $payrollItem->TotalCreditAmount, 1);
            $gl->add_gl_trans(0,  $typeNo, $payrollItem->BankAccount, "Payroll Payments", -$payrollItem->TotalCreditAmount,  "", "");
            foreach($payrollItem->DebitItems as $glItems){
                $gl->add_gl_trans(0,  $typeNo, $glItems->ItemCode, $glItems->Memo, $glItems->ItemAmount,  null, null);
            }

        } 
        echo json_encode(array("code=>200", "info"=>"Entries posted successfully"));
    }
    
}else{
    echo json_encode(array("code=>500", "info"=>"Entries Failed check post data and try again"));
 
 }

?>
