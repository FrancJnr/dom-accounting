<?php

header("Access-Control-Allow-Origin: *");
$path_to_root = "../../";
// include_once($path_to_root . "/includes/session.inc");
include("../db/connect_db.php");
include("../db/voucher.php");


$db = new Database();
$conn = $db->getConnection();

$sale = new Sales($db->getConnection());

$action = isset($_POST["action"]) ? $_POST["action"] : "";
$ref = isset($_POST["amount"]) ? $_POST["amount"] : "";
$id = isset($_POST["id"]) ? $_POST["id"] : "";
$fieldValue = isset($_POST["fieldValue"]) ? $_POST["fieldValue"] : "";
$fieldName = isset($_POST["fieldName"]) ? $_POST["fieldName"] : "";
$status = isset($_POST["status"]) ? $_POST["status"] : "";
$reference = isset($_POST["reference"]) ? $_POST["reference"] : "";
$invoice_date = isset($_POST["invoice_date"]) ? $_POST["invoice_date"] : "";
$user_id = isset($_POST["user_id"]) ? $_POST["user_id"] : "";
$user_name = isset($_POST["user_name"]) ? $_POST["user_name"] : "";

$chequeno = isset($_POST["chequeno"]) ? $_POST["chequeno"] : "";
$idno = isset($_POST["idno"]) ? $_POST["idno"] : "";
$payee = isset($_POST["payee"]) ? $_POST["payee"] : "";
$account = isset($_POST["account"]) ? $_POST["account"] : "";
$narrative = isset($_POST["description"]) ? $_POST["description"] : "";

switch ($action) {
   case 'get-invoices':
      echo get_invoice($conn);
      break;
   case 'add-record':
      echo addRecord($conn);
      break;
   case 'get-dept':
    echo fetchItems($conn);
      break;
   case 'update-field':
    echo update_value($conn, $id, $fieldValue, $fieldName);
      break;
   case 'cancel-invoice':
    echo cancelInvoice($conn);     
      break;
   case 'delete-row':
    echo deleteRow($id, $conn);    
    break;
   case 'post-invoice':
      $sale->user_name=$user_name;
      $sale->user_id =$user_id;
      $sale->reference = $reference;
      $sale->trans_date = $invoice_date;
      $sale->cheq_no = $chequeno;
      $sale->payee = $payee;
      $sale->account = $account;
      $sale->type = 1;
      $sale->supplier_id = null;
      $sale->person_type_id = null;
      $sale->amount = $amount;
      $sale->bank_account = $account;
      $sale->narrative = $narrative;
      $sale->idno = $idno;
      $sale->chequeno =$chequeno;
      $sale->transno = $sale->getNextTranNo();
      $sale->items = $sale->items();
      $sale->addToApprovalWorkflow();
      // echo $sale->post_pos_sale();
      break;
   case 'approve-payment':
      if(isset($_POST["approval-level"]) && $_POST["approval-level"] == "admin"){
         $sale->postVoucher();
      }elseif(isset($_POST["approval-level"]) && $_POST["approval-level"] == "subadmin"){
         $id = isset($_POST["id"]) ? $_POST["id"] : die("missing id");
         $sale->subadminApproval($id);
      }

   break;

   case 'next_ref':
      echo json_encode(array("next_ref"=>getNextRef($conn)));
      break;

   case 'payee':
      $type = isset($_POST["type"]) ? $_POST["type"] : '';
      echo  getPayee($conn, $type);
      break;
   case 'accounts':
      echo bankAccounts($conn);
      break;
   default:
      # code...
      break;

      
}

 function get_invoice($conn){
     try{
      $sql = "SELECT * FROM 0_voucher_temp WHERE  posted = false";
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      $sales = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $totalamount = 0;
      $totalInvoice = 0;
      $totalVat = 0;
      $totalLevy = 0;
      $output = '
      <table class="table table-striped table-hover table-bordered table-responsive">
		<thead class="thead-dark">
			<th>Expense Account</th>
         <th>Code</th>
			<th>Amount</th>
			<th></th>
		</thead>
        <tbody>';
      foreach($sales as $sale){
        $totalamount += $sale['amount'];
      
          $id = $sale['id'];
          $item = $sale['item'];
         $output.='<tr>';
         if($item != null){
            $output.='<td>'.$item.'</td>';
         }else{
            $output.='<td id="'.$id.'" class="dept"></td>';
         }
         
         $output.='<td><input disabled="true" class="form-control"  placeholder="" value="'.$sale['code'].'"></td>';
         $output.='<td class="'.$sale['tax_type_id'].'" id="'.$id.'"><input name="amount" class="form-control amount"  placeholder="" value="'.$sale['amount'].'"></td>';
         $output.='<td>
                  <span>
                     <button class="btn btn-danger"><i id="'.$id.'" class="remove fa fa-minus" style="color:white"></i></button>
                  </span>
                  </td>';
            }
         $output.='
         </tr>';
		$output.='<tbody>
        <tfoot>';
            $output.= '<tr>';
                $output.= '<td></td>';     
                $output.= '<td></td>';       
                $output.= '<td></td>';              
                $output.= '<td><button id="add" class="btn btn-success btn-sm text" style="font-size:smaller;"><i class="fa fa-plus" style="color:white"></i></button></td>';       

            $output.='</tr>';
         $output.= '<tr>';
            $output.= '<td>Cheque No</td>';     
            $output.= '<td colspan="1" rows="4" cols="50"><input name="chequeno" class="form-control chequeno"  id="chequeno" placeholder=""></td>';       
        $output.='</tr>';
            $output.= '<tr>';
                $output.= '<td>ID NO</td>';     
                $output.= '<td><input name="amount" class="form-control"  id="idno" placeholder=""></td>';       
            $output.='</tr>';
            $output.= '<tr>';
                $output.= '<td>Memo</td>';     
                $output.= '<td colspan="1" rows="4" cols="50" class="description">
                              <textarea></textarea></td>';       
            $output.='</tr>';
            $output.='<tr>';

               $output.= '<td>Total</td>';     
               $output.= '<td></td>';                
               $output.= '<td>'.$totalamount.'</td>';       

               $output.='</tr>';
            $output.='</tfoot>
        </table>';
     }catch(Exception $ex){
        var_dump($ex);
     }
     return $output;

}

function date2sql($date_)
{
    $date = strtotime($date_);
    return date('Y-m-d', $date);
	
}

function addRecord($conn){
   try {
      $sql = "INSERT INTO `0_voucher_temp`(`code`) VALUES(null)";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

      
   } catch (\Throwable $th) {
      throw $th;
   }

   return json_encode(array("added"=>"true"));

}

function update_value($conn, $id, $fieldValue, $fieldName){
   try {
      $sql = "UPDATE 0_voucher_temp SET $fieldName = '$fieldValue' WHERE id = $id";
      $stmt = $conn->prepare($sql);
      $stmt->execute();
   } catch (\Throwable $th) {
      //throw $th;
   }
   return json_encode(array("updated"=>"true"));


}
function post_invoice($conn, $id, $gl){

   
      echo json_encode(array("status"=>"Transactions Successfully Posted"));
   
   

}
function deleteRow($id, $conn){
   try {
      $sql = "DELETE FROM 0_voucher_temp WHERE id = $id";
      $stmt = $conn->prepare($sql);
      $stmt->execute();

   } catch (Exception $th) {
      echo($th);
   }
   return json_encode(array("deleted"=>"true"));

}
function cancelInvoice($conn){
    try {
       $sql = "DELETE FROM 0_voucher_temp";
       $stmt = $conn->prepare($sql);
       $stmt->execute();
 
    } catch (Exception $th) {
       echo($th);
    }
    return json_encode(array("deleted"=>"true"));
 
 }
function fetchItems($conn){
    try {
       $sql = "SELECT * FROM `0_stock_master`";
       $stmt = $conn->prepare($sql);
       $stmt->execute();
       $items = $stmt->fetchAll(PDO::FETCH_ASSOC);

       $output = "";
       $output.='<select class="form-control dept-select" name="item">';
       $output.='<option>SELECT</option>';
       foreach ($items as $item) {
           $output.='<option class="'.$item['tax_type_id'].'" id='.$item['stock_id'].'>'.$item['description'].'</option>';
       }
       $output.='</select>';
 
    } catch (Exception $th) {
       echo($th);
    }
    return $output;
 
}
function getNextRef($conn){
   try {
      $sql = "SELECT (CASE WHEN max(reference) IS NULL THEN LPAD(1,3,'0') ELSE  LPAD(MAX(reference+1), 3, '0') END) AS next_ref FROM 0_refs WHERE type = 10";
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      $row = $stmt->fetch();

      return $row["next_ref"];

   } catch (Exception $th) {
      var_dump($th);
   }
}
function getPayee($conn, $type){
   try {
      $sql = "";
      if($type=="supplier"){
         $sql = "SELECT supplier_id AS id, supp_name AS name  FROM 0_suppliers WHERE inactive = 0";
         $stmt = $conn->prepare($sql);
         $stmt->execute();
         $items = $stmt->fetchAll(PDO::FETCH_ASSOC);

         $output = "";
         $output.='<select class="form-control payee-select" name="payee">';
         $output.='<option>SELECT</option>';
         foreach ($items as $item) {
            $output.='<option  id='.$item['id'].'>'.$item['name'].'</option>';
         }
         $output.='</select>';
      }else if($type=="customer"){
         $sql = "SELECT debtor_no AS id,  name  FROM 0_debtors_master WHERE inactive = 0";
         $stmt = $conn->prepare($sql);
         $stmt->execute();
         $items = $stmt->fetchAll(PDO::FETCH_ASSOC);

         $output = "";
         $output.='<select class="form-control payee-select" name="payee">';
         $output.='<option>SELECT</option>';
         foreach ($items as $item) {
            $output.='<option  id='.$item['id'].'>'.$item['name'].'</option>';
         }
         $output.='</select>';
      }else{
        $output= '<input class="form-control payee-input" type="text">';
      }
      


   } catch (Exception $th) {
      var_dump($th);
   }
   return $output;

}
function bankAccounts($conn){
   try {
      $sql = "";
         $sql = "SELECT * FROM `0_bank_accounts` WHERE inactive = 0 AND account_type = 3";
         $stmt = $conn->prepare($sql);
         $stmt->execute();
         $accounts = $stmt->fetchAll(PDO::FETCH_ASSOC);

         $output = "";
         $output.='<select class="form-control payee-select" id="account_code" name="payee">';
         $output.='<option>Petty Cash Account</option>';
         foreach ($accounts as $account) {
            $output.='<option  id='.$account['account_code'].'>'.$account['bank_account_name'].'</option>';
         }
         $output.='</select>';
     
   } catch (Exception $th) {
      var_dump($th);
   }
   return $output;

}
?>
