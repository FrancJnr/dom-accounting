<?php

class Gl{
  
    // database connection and table name
	private $conn;
	public $tbpref;
       
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
	}

	public function add_to_journal($type,  $trans_no, $trans_date, $reference, $source_ref,  $eventdate, $doc_date, $currency, $amount, $rate){
		try {
			$sql2 = "SELECT max(trans_no) AS type_no FROM 0_journal";
			$stmt2 = $this->conn->prepare($sql2);
			$stmt2->execute();
			while($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
				$typeNo = $row['type_no'];
			}
			$source_ref="JENTRY";
			$typeNo=$typeNo+1;
			$sql="INSERT INTO `0_journal`(`type`, `trans_no`, `tran_date`, `reference`, `source_ref`, `event_date`, `doc_date`, `currency`, `amount`, `rate`) 
			VALUES (?,?,current_date,?,?,current_date,current_date,?,?,?)";
			$stmt = $this->conn->prepare($sql);
			$stmt->bindValue(1, 0);
			$stmt->bindParam(2, $typeNo);	
			$stmt->bindParam(3, $reference);
			$stmt->bindParam(4, $source_ref);
			$stmt->bindParam(5, $currency);
			$stmt->bindParam(6, $amount);
			$stmt->bindParam(7, $rate);
			$stmt->execute();
	        return $typeNo;
		} catch (Exception $ex) {
			var_dump($ex);
		}
	}
	public function add_gl_trans($type,  $typeno, $account, $memo, $amount,  $person_type_id, $person_id){
		try {
			$sql2 = "SELECT max(type_no) AS type_no FROM 0_gl_trans";
			$stmt2 = $this->conn->prepare($sql2);
			$stmt2->execute();
			while($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
				$typeNo = $row['type_no'];
			}
			$typeNo=$typeNo+1;
			$sql="INSERT INTO 0_gl_trans(`type`, `type_no`, `tran_date`,`account`, `memo_`, `amount`) 
			VALUES (?,?,now(),?,?,?)";
			$stmt = $this->conn->prepare($sql);
			$stmt->bindParam(1, $type);
			$stmt->bindParam(2, $typeno);	
            $stmt->bindParam(3, $account);
			$stmt->bindParam(4, $memo);
			$stmt->bindParam(5, $amount);
		
			
			$stmt->execute();
	     
		} catch (Exception $ex) {
			var_dump($ex);
		}
	}

}
