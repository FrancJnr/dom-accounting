<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


	$page_security = 'SA_SETUPDISPLAY'; // A very low access level. The real access level is inside the routines.
	$path_to_root = "../../";

	include_once($path_to_root . "/includes/session.inc");
	include_once($path_to_root . "/includes/ui.inc");
	include_once($path_to_root . "/admin/db/fiscalyears_db.inc");
	include_once($path_to_root . "/includes/date_functions.inc");
	include_once($path_to_root . "/includes/data_checks.inc");
    include_once($path_to_root . "/gl/includes/gl_db.inc");
    include_once($path_to_root . "/api/db/voucher_approval_db.php");
	include_once($path_to_root . "/api/db/connect_db.php");

    page(_($help_context = "Pending Approvals"), false, false, "", $js);

	
?>
<?php
	$base_url = explode('?', $_SERVER[REQUEST_URI], 2)[1];
	$paramC = explode('&', $base_url , 2)[0];
	$paramD = explode('&', $base_url , 2)[1];

	  $cashierId = 1;
	  if ($_GET['C'] !==null){
		$cashierId = $_GET['C'];
		
	 }
	 $date = date("Y-m-d");// current date

	 if ($_GET['D'] !==null){
		$date = $_GET['D'];
		if($date == 'today'){
			$date = date("Y-m-d");
		}
		
     }
     $db = new Database();
	 $voucher = new Voucher($db->getConnection());
	
	 $logedinrole = $voucher->check_role($_SESSION['wa_current_user']->user)['role_id'];
	 $row= $voucher->fetch_pending_approvals($logedinrole);
	


	 $columnid = -1;

function user_actions($approvestatus, $rejectstatus, $btnid){
		$actions= '
		<div style="padding-right:15px;" id="outer">
			<div class="inner" >
				<button id="'.$btnid.'" type="button" value="1" class="btn btn-success">'.$approvestatus.'</button>								
			</div>
			<div class="inner">  
				<button id="'.$btnid.'" type="button" value="0" class="btn btn-danger">'.$rejectstatus.'</button>									
			</div>
		</div>';
	return $actions;

}
function decode_approval($input){
	if($input==1){
		return "Yes";
	}else if($input==null){
		return "Pending";
	}else{
		return "Rejected";
	}
}

function approval_view($vtype, $row){
if($vtype==2 || $vtype==10 || $vtype==9){
	$id = 0;
	$html='<div class="container-fluid">
   
	<div class="row">

	  <div class="col-md-10">
	  <table style="width:100%; background-color:alice-blue;">
	  <tr>
		  <td>
			  <table id="vouchers" class="display" style="width:50%;">
			  <thead class="grey lighten-2">
				  <tr>
					  <th scope="col">#</th>
					  <th scope="col">Date</th>
					  <th scope="col">Raised By</th>
					  <th scope="col">Supplier/Customer</th>
					  <th scope="col">Withdrawn Account</th>
					  <th scope="col">Amount</th>
					  <th scope="col">Cheque No</th>
					  <th scope="col">Payment Reason</th>
					  <th scope="col">Admin Approval</th>
					  <th scope="col">Sub Admin Approval</th>
					  <th scope="col">Actions</th>
				  </tr>
			  </thead>
			  <tbody>';  	 	 					
							  foreach($row as $myrow){
								  $id+=1;
									  $html.= "<tr>";
									  $html.="<td>".$id."</td>";
									  $html.="<td>".$myrow["trans_date"]."</td>";
									  $html.="<td>".$myrow['real_name']."</td>";
									  $html.="<td>".$myrow['supp_name']."</td>";
									  $html.="<td>".$myrow['bank_account_name']."</td>";
									  $html.="<td>".abs($myrow['amount'])."</td>";
									  $html.="<td>".$myrow['cheque_no']."</td>";
									  $html.="<td>".$myrow['narrative']."</td>";
									  $html.="<td>".decode_approval($myrow['aproved_by_admin'])."</td>";
									  $html.="<td>".decode_approval($myrow['aproved_by_sub_admin'])."</td>";
									  if($myrow['aproved_by_sub_admin']==1 && $myrow['aproved_by_admin']==1){

										$html.="<td><a target='_blank' href='../../reporting/prn_redirect.php?PARAM_0=".$myrow["trans_no"]."-".$myrow["type"]."&amp;PARAM_1=".$myrow["trans_no"]."-".$myrow["type"]."&amp;PARAM_2=&amp;PARAM_3=0&amp;PARAM_4=&amp;PARAM_5=0&amp;REP_ID=210' id='_el60337750610aa7.39285477' class='printlink'><img src='../../themes/canvas/images/print.png' style='vertical-align:middle;width:12px;height:12px;border:0;'
										 title='Print Remittance'>
										</a></td>";
										// <a target="_blank" href="../../reporting/prn_redirect.php?PARAM_0=1-22&amp;PARAM_1=1-22&amp;PARAM_2=&amp;PARAM_3=0&amp;PARAM_4=&amp;PARAM_5=0&amp;REP_ID=210" id="_el60335bea841935.39655725" class="printlink"><img src="../../themes/canvas/images/print.png" style="vertical-align:middle;width:12px;height:12px;border:0;" title="Print Remittance">
										// </a>
										
									  }else{
										$html.="<td>Voucher Not Ready</td>";
									  }

									  $html.="</tr>";
									  

								  }							
				  
			  $html.="</tbody>";
			  $html.="</table>";
			  $html.="</tr>";
	  $html.="</table>";
	  $html.="</div>";

	  $html.="</div>";
	  echo $html;

	}else{
		$html='<div class="container-fluid well" style="background-color:gray; color:red; height:200px; width:400px; border-color:red;>
			<p>Ooops! you dont have the perimission to view the voucher approval page. You have to have the role of admin or Subadmin</p>
		</div>';
		echo $html;

	}


}

?>
<html lang="en">
  <head>
		<style>
				.error{
					background-color:red;
				}
				.success{
					background-color:green;
				}
		    	#vouchers{
					width:100%;
				}
                .inner{
                    display: inline-block;
					width:80px;
           		 }
				
				.sorting{
					color:white;
					margin-left:0px;
				}
				table.dataTable thead tr {
				background-color: green;
				color:white !important;
				}
				#vouchers {
				font-family: Arial, Helvetica, sans-serif;
				border-collapse: collapse;
				width: 100%;
				}

				#vouchers td, #vouchers th {
				border: 1px solid #fff;
				/* padding: 0px; */
				}

				#vouchers tr:nth-child(even){background-color: #90c1e8;}
				#vouchers tr:nth-child(odd){background-color: #ddd;}

				#vouchers tr:hover {background-color: #ffff;}

				#vouchers td, #vouchers th {
				border: 1px solid #fff;
				/* padding: 5px; */
				}

				#vouchers tr:nth-child(even){background-color: #90c1e8;}
				#vouchers tr:nth-child(odd){background-color: #ddd;}

				#vouchers tr:hover {background-color: #ffff;}

				#notification {
					position:fixed;
					top:20%;
					width:80%;
					z-index:105;
					text-align:center;
					font-weight:normal;
					font-size:14px;
					font-weight:bold;
					color:white;
					/* background-color:#8be09f; */
					padding:5px;
				}
				#notification span.dismiss {
					border:2px solid #FFF;
					padding:0 5px;
					cursor:pointer;
					float:right;
					margin-right:10px;
				}
				#notification a {
					color:white;
					text-decoration:none;
					font-weight:bold
				}
				.overlay{
					display: none;
					position: fixed;
					width: 100%;
					height: 100%;
					top: 0;
					left: 0;
					z-index: 999;
					background: rgba(255,255,255,0.8) url("/erp/themes/canvas/images/ajax-loader.gif") center no-repeat;
					
				}
				body{
					text-align: center;
				}
				/* Turn off scrollbar when body element has the loading class */
				body.loading{
					overflow: hidden;   
				}
				/* Make spinner image visible when body element has the loading class */
				body.loading .overlay{
					display: block;
				}

		
		</style>

  </head>

  <body class="loading">
		<?php
			approval_view($logedinrole, $row);
		?>
		<div id="notification" style="display: none;">
			<span class="dismiss"><a title="dismiss this notification">x</a></span>
		</div>
  </body>   
	<!-- DataTables -->
		<link rel="stylesheet" href="/erp/assets/DataTables-1.10.23/media/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="/erp/assets/DataTables-1.10.23/media/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="/erp/assets/bootstrap-3.4.1-dist/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/erp/assets/bootstrap-3.4.1-dist/css/bootstrap.min.css">

        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/jquery-3.5.1.min.js"></script>
        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/jquery.js"></script>
        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="/erp/assets/DataTables-1.10.23/media/js/dataTables.bootstrap5.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#vouchers').DataTable( {
				"paging":   false,
				"ordering": true,
				"info":     false,
				"scrollY": 400,
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				],
			
				"autoWidth": false
			});
			
		} );
	
	</script>
	<script>
		$(document).on({
			ajaxStart: function(){
				$("body").addClass("loading"); 
			},
			ajaxStop: function(){ 
				$("body").removeClass("loading"); 
			}    
		});
	
	</script>
		<script>
		$('button').on('click', function(e){
			e.preventDefault();
			var data = {};        
			var roleid=<?php echo $logedinrole?>;
			var value = $(this).attr('value')
			data.pk = $(this).attr('id');
			data.role = roleid;
			data.value = value;

			console.log($(this).attr('id'));

			$.ajax({
				type: "POST",
				url: "voucher_approval.php",
				data: data,
			}).done(function(data) {
					if(data.approved=="1"){
						$("#notification").addClass("success");
						$("#notification").fadeIn("slow").append(data.status);
						$(".dismiss").click(function(){
							$("#notification").fadeOut("slow");
						});
						window.setTimeout(function(){location.reload()},2000)

					}else{
						$("#notification").addClass("error");
						$("#notification").fadeIn("slow").append(data.status);
						$("#notification").addClass("error");
						$(".dismiss").click(function(){
							$("#notification").fadeOut("slow");
						});
						window.setTimeout(function(){location.reload()},2000)

						console.log("cannot Approve");
					}
			});
		});

	</script>



	<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script> -->
	
 
</html>