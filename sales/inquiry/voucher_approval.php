<?php

/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
header('Content-Type: application/json');

$path_to_root = "../..";


include_once($path_to_root . "/api/db/voucher_approval_db.php");
include_once($path_to_root . "/api/db/connect_db.php");
include_once($path_to_root . "/includes/main.inc");
include_once($path_to_root . "/includes/session.inc");

    $db = new Database();

    $user_id =$_SESSION["wa_current_user"]->user;
    $voucher = new Voucher($db->getConnection());
    $approvaltype = $_POST['id'];
    $can_approve = $voucher->check_if_can_approve($user_id, $approvaltype);
    echo json_encode(array('canapprove' => $can_approve));
    exit;

?>