<?php

/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your optionion) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
 ***********************************************************************/
//-----------------------------------------------------------------------------
//
//	Entry/Modify Sales Quotations
//	Entry/Modify Sales Order
//	Entry Direct Delivery
//	Entry Direct Invoice
//

$path_to_root = "..";
$page_security = 'SA_SALESORDER';

include_once($path_to_root . "/sales/includes/cart_class.inc");
include_once($path_to_root . "/includes/session.inc");
// include_once($path_to_root . "/sales/includes/sales_ui.inc");
// include_once($path_to_root . "/sales/includes/ui/sales_order_ui.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");
include_once($path_to_root . "/sales/includes/db/sales_types_db.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");


$user_id = $_SESSION["wa_current_user"]->user;
$user_name = $_SESSION["wa_current_user"]->username;


page("Raise Payment Vouchers", false, false, "", "");

$js_static = array();
?>

<html>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous">
</script>
<script src="https://use.fontawesome.com/40b461d056.js"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<div class="card" style="margin-top: 5vH;">
	<div class="row text-center">
		<div class="col-md-2">
			<div class="form-group">
				<label for="dept">Voucher</label>
				<input id="reference" name="reference"></input>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<input style="position:inline;" id="datepicker" placeholder="Invoice Date" width="276" />
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<div class="col-sm-10" name="account" id="account">
				</div>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<div class="col-sm-10">
					<select class="form-control-plaintext" name="payto" id="payto">
						<option value="">Pay To</option>
						<option value="supplier">Supplier</option>
						<option value="customer">Customer/Staff</option>
					</select>
				</div>
			</div>
  		</div>
		  <div class="col-md-2">
			<div class="form-group">
				<div class="col-sm-10" name="payee" id="payee">
			
				</div>
			</div>
		</div>
	</div>


</div>
<div class="card" id="invoiceCard">
	<div class="card-header">
	</div>
	<div class="card-header">
	</div>
	<div class="card-body text-center">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div style="width:120%;" id="invoiceTable"></div>
			</div>
		</div>
		


	</div>
	<div class="card-footer text-center">
		<div class="row justify-content-center">
			<div class="col-auto">
				<button id="place" class="btn btn-success btn-sm"> <i class="fa fa-check"></i>Raise Voucher</button>
				<button id="cancel" class="btn btn-danger btn-sm"> <i class="fa fa-times"></i>Cancel Voucher</button>
			</div>
		</div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js" integrity="sha512-9p/L4acAjbjIaaGXmZf0Q2bV42HetlCLbv8EP0z3rLbQED2TAFUlDvAezy7kumYqg5T8jHtDdlm1fgIsr5QzKg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

<script>
	$(document).ready(function() {
		loadRef();
		loadTransTable();
		loadPayee("");
		$.ajax({
			url: "../api/endpoints/sale_invoicing.php",
			type: "POST",
			dataType: "html",
			data: {
				action: 'accounts',
			},
			success: function(data) {
				$("#account").html(data);
			}
		});


	});

	function loadTransTable() {
		$.ajax({
			url: "../api/endpoints/sale_invoicing.php",
			type: "POST",
			dataType: "html",
			data: {
				action: "get-invoices"
			},
			success: function(data) {
				$("#invoiceTable").html(data);
				loadDepartments();

			}
		});
	}

	function getTotalAmount(ref) {
		$.ajax({
			url: "../api/endpoints/sale_invoicing.php",
			type: "POST",
			dataType: "html",
			data: {
				action: "get-total",
				ref: ref
			},
			success: function(data) {
				console.log(data);
			}
		});
	}

	function updateField(fieldName, fieldValue, id) {
		$.ajax({
			url: "../api/endpoints/sale_invoicing.php",
			type: "POST",
			dataType: "html",
			data: {
				action: "update-field",
				id: id,
				fieldName: fieldName,
				fieldValue: fieldValue
			},
			success: function(data) {
				loadTransTable();
				console.log(data);
			}
		});
	}
	$("body").on("focusout", ".amount", function(e) {
		var amount = $(this).val();
		var id = $(this).parent().attr("id");
		var fieldName = "amount";
		updateField(fieldName, amount, id);
	});
	$("#cancel").click(function(e) {
		e.preventDefault();
		$.ajax({
			url: "../api/endpoints/sale_invoicing.php",
			type: "POST",
			dataType: "json",
			data: {
				action: 'cancel-invoice'
			},
			success: function(data) {
				alert("invoice canceled");
				loadTransTable();

			}
		});
	});
	$("body").on("click", "#add", function(e) {
		e.preventDefault();
		$.ajax({
			url: "../api/endpoints/sale_invoicing.php",
			type: "POST",
			dataType: "json",
			data: {
				action: 'add-record'
			},
			success: function(data) {
				loadTransTable();

			}
		});
	});
	$("#place").click(function(e) {
		var reference =  $("#reference").val();
		var invoice_date = $("#datepicker").val();
		var payee =  $("#payee-input").val();

		var payto =  $("#payto").val();
		var chequeno =  $("#chequeno").val();
		var idno =  $("#idno").val();
		var account =  $("#account_code").val();

		
		var user_id = '<?php echo $user_id ?>';
		var user_name = '<?php echo $user_name ?>';

		if((reference != null) && (invoice_date != null)){
				$.ajax({
				url: "../api/endpoints/sale_invoicing.php",
				type: "POST",
				dataType: "json",
				data: {
					action: 'post-invoice',
					reference: reference,
					invoice_date: invoice_date,
					user_id: user_id,
					user_name: user_name,
					recepient: "Francis Chege",
					payto: payto,
					chequeno:chequeno,
					idno:idno,
					account:account
				},
				success: function(data) {
					$("#invoiceCard").html('<span>Voucher posted Successfully </span>');
				}
			});
		}else{
			alert("Missing Reference or Date");
		}
		
	})
	$("body").on("click", ".remove", function(e) {
		e.preventDefault();
		var id = $(this).attr('id');
		$.ajax({
			url: "../api/endpoints/sale_invoicing.php",
			type: "POST",
			dataType: "json",
			data: {
				id: id,
				action: 'delete-row'
			},
			success: function(data) {
				loadTransTable();
			}
		});
	})
	$("body").on("change", ".dept-select", function(e) {
		e.preventDefault();
		var fieldValue = $(this).val();
		var id = $(this).parent().attr("id");
		var fieldName = $(this).attr("name");
		var fieldKey = $(this).find(":selected").attr("id");
		var taxType = $(this).find(":selected").attr("class");

		updateField(fieldName, fieldValue, id);
		updateField("code", fieldKey, id);
	});
	$("#payto").on("change", function(e) {
		e.preventDefault();
		var type = $(this).val();
		loadPayee(type);
	});
	$('#datepicker').datepicker({ format: 'yyyy-mm-dd', value: '2021-10-23' });

	function loadDepartments() {
		$.ajax({
			url: "../api/endpoints/sale_invoicing.php",
			type: "POST",
			dataType: "html",
			data: {
				action: "get-dept",

			},
			success: function(data) {
				$(".dept").html(data);
			}
		});
	}
	function loadRef(){
		$.ajax({
			url: "../api/endpoints/sale_invoicing.php",
			type: "POST",
			dataType: "json",
			data: {
				action: 'next_ref'
			},
			success: function(data) {
				$("#reference").val(data.next_ref);
			}
		});
	}
	function loadPayee(type){
		$.ajax({
			url: "../api/endpoints/sale_invoicing.php",
			type: "POST",
			dataType: "html",
			data: {
				action: 'payee',
				type : type
			},
			success: function(data) {
				$("#payee").html(data);
			}
		});
	}
</script>

</html>

